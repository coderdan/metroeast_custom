<?php
/**
 * @file
 * metroeast_file_display.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function metroeast_file_display_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
}
