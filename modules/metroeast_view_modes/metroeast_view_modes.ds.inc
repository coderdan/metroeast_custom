<?php
/**
 * @file
 * metroeast_view_modes.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function metroeast_view_modes_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'button';
  $ds_view_mode->label = 'Button';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['button'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'circle_116';
  $ds_view_mode->label = 'Circle 116';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['circle_116'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_42';
  $ds_view_mode->label = 'Teaser 42';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['teaser_42'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_half';
  $ds_view_mode->label = 'Teaser Half';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['teaser_half'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_quarter';
  $ds_view_mode->label = 'Teaser Quarter';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['teaser_quarter'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_thumb';
  $ds_view_mode->label = 'Teaser Thumb';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['teaser_thumb'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_tiny';
  $ds_view_mode->label = 'Teaser Tiny';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'file' => 'file',
  );
  $export['teaser_tiny'] = $ds_view_mode;

  return $export;
}
