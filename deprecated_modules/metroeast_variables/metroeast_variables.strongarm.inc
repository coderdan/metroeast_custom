<?php
/**
 * @file
 * metroeast_variables.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function metroeast_variables_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = TRUE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'civicrm_class_loader';
  $strongarm->value = '/Volumes/Storage/Sites/dev4.metroeast.org/sites/all/modules/civicrm/drupal/../CRM/Core/ClassLoader.php';
  $export['civicrm_class_loader'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'contact_default_status';
  $strongarm->value = 1;
  $export['contact_default_status'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_db_tz_support';
  $strongarm->value = FALSE;
  $export['date_db_tz_support'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_default_timezone';
  $strongarm->value = 'America/Los_Angeles';
  $export['date_default_timezone'] = $strongarm;

  return $export;
}
