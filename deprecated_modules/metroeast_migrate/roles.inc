<?php

/**
 * @file
 * Migration classes for Roles
 *
 */

/**
 *  MetroEastRoleMigration implementation of DrupalRole7Migration
 */
class MetroEastRoleMigration extends DrupalRole7Migration {
  public function __construct(array $arguments) {

    parent::__construct($arguments);

  }

  /**
   * Implements query()
   * @see https://drupal.org/node/1819738
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $query = parent::query();

    // Exclude administrator account since its added by the install profile
    $query->condition('name', 'administrator', 'NOT LIKE');

    return $query;
  }

}
