<?php

/**
 * @file
 *
 */

class MetroEastVideoMigration extends  MigrateDestinationMedia {
  public function __construct(array $arguments) {
    parent::__construct($arguments);


    $this->destination = new MigrateDestinationMedia('video',
       'MigrateExtrasFileYoutube');
        // Clear references to normal file fields
    $this->removeFieldMapping('destination_dir');
    $this->removeFieldMapping('source_dir');
    $this->removeFieldMapping('file_replace');
    $this->removeFieldMapping('preserve_files');
    $this->removeFieldMapping('destination_file');

  }
}