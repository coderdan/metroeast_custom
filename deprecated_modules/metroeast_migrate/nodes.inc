<?php

/**
 * @file
 * Migration classes for nodes
 */
class MetroEastArticleNodeMigration extends DrupalNode7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('field_tags', 'field_tags')
             ->sourceMigration('MetroEastArticleTagTerm')
             ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_image', 'field_image')
         ->sourceMigration('MetroEastFiles');

    $this->addFieldMapping('field_image:file_class')
         ->defaultValue('MigrateFileFid');

    // And we map the alt and title values in the database to those on the image.
    $this->addFieldMapping('field_image:alt', 'image_alt');
    $this->addFieldMapping('field_image:title', 'image_title');

    $this->addFieldMapping('field_video', 'field_video')
          ->sourceMigration('MetroEastVideos');

    $this->addFieldMapping('field_video:file_class')
          ->defaultValue('MigrateFileFid');

    $this->addFieldMapping('field_section', 'field_section');

    $this->addFieldMapping('field_reports', 'field_reports');

  }
}

class MetroEastReservationBucketNodeMigration extends DrupalNode7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

  }

  /**
   * Implements complete()
   * @param $entity
   *  - the saved (destination) migration object
   * @param $row
   *  - the original source row
   *
   * Reservations require additional settings (tables) be migrated along with the
   * destination node. While the Reservations module does create these records
   * automatically during the migration, the values are only the defaults as defined in
   * the Reservations module. The original values need to be migrated into the
   * tables, but with updated nid an vid values.
   *
   */
  function complete ($entity, stdClass $row) {

    db_set_active($this->sourceConnection);

    // fetch the source values from reservations_bucket_node
    $bucket_result = db_query('SELECT * FROM {reservations_bucket_node} WHERE nid = :nid', array(':nid' => $row->nid));
    $bucket_row = $bucket_result->fetchAssoc();
    // replace nid and vid values with those created from this migration
    $bucket_row['nid'] = $entity->nid;
    $bucket_row['vid'] = $entity->vid;

    // fetch the source values from reservations_reservation_item_node
    $item_result = db_query('SELECT * FROM {reservations_reservation_item_node} WHERE nid = :nid', array(':nid' => $row->nid));
    $item_row = $item_result->fetchAssoc();
     // replace nid and vid values with those created from this migration
    $item_row['nid'] = $entity->nid;
    $item_row['vid'] = $entity->vid;

    // fetch the source values from reservations_node_type
    $type_result = db_query('SELECT * FROM {reservations_node_type} r WHERE r.type = :type', array(':type' => $this->destinationType));
    $type_row = $type_result->fetchAssoc();

    db_set_active();

    // update the 'reservations_bucket_node' table
    $update_bucket = db_update('reservations_bucket_node') // Table name no longer needs {}
      ->fields($bucket_row)
      ->condition('nid', $entity->nid, '=')
      ->execute();

    // update the reservations_reservation_item_node table
    $update_bucket = db_update('reservations_reservation_item_node') // Table name no longer needs {}
      ->fields($item_row)
      ->condition('nid', $entity->nid, '=')
      ->execute();


    // update the reservations_node_type table
    $update_bucket = db_update('reservations_node_type') // Table name no longer needs {}
      ->fields($type_row)
      ->condition('type', $type_row['type'], '=')
      ->execute();
  }
}

class MetroEastReservationResoureNodeMigration extends DrupalNode7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

  }

  function complete ($entity, stdClass $row) {

    db_set_active($this->sourceConnection);

    // fetch the source values from reservations_bucket_node
    $bucket_result = db_query('SELECT * FROM {reservations_resource_node} WHERE nid = :nid', array(':nid' => $row->nid));
    $bucket_row = $bucket_result->fetchAssoc();
    // replace nid and vid values with those created from this migration
    $bucket_row['nid'] = $entity->nid;
    $bucket_row['vid'] = $entity->vid;

    // fetch the source values from reservations_reservation_item_node
    $item_result = db_query('SELECT * FROM {reservations_reservation_item_node} WHERE nid = :nid', array(':nid' => $row->nid));
    $item_row = $item_result->fetchAssoc();
    // replace nid and vid values with those created from this migration
    $item_row['nid'] = $entity->nid;
    $item_row['vid'] = $entity->vid;

    // fetch the source values from reservations_node_type
    $type_result = db_query('SELECT * FROM {reservations_node_type} r WHERE r.type = :type', array(':type' => $this->destinationType));
    $type_row = $type_result->fetchAssoc();

    db_set_active();

    // update the 'reservations_resource_node' table
    $update_bucket = db_update('reservations_resource_node') // Table name no longer needs {}
      ->fields($bucket_row)
      ->condition('nid', $entity->nid, '=')
      ->execute();

    // update the reservations_reservation_item_node table
    $update_bucket = db_update('reservations_reservation_item_node') // Table name no longer needs {}
      ->fields($item_row)
      ->condition('nid', $entity->nid, '=')
      ->execute();

    // update the reservations_node_type table
    $update_bucket = db_update('reservations_node_type') // Table name no longer needs {}
      ->fields($type_row)
      ->condition('type', $type_row['type'], '=')
      ->execute();

  }
}

class MetroEastMultidayEventNodeMigration Extends DrupalNode7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->addFieldMapping('field_civievent_id', 'field_civievent_id');
    $this->addFieldMapping('field_civievent_template', 'field_civievent_template');
    $this->addFieldMapping('field_civicrm_multiday_session', 'field_civicrm_multiday_session');
    $this->addFieldMapping('field_date_of_first_session', 'field_date_of_first_session');
    $this->addFieldMapping('field_civicrm_multiday_session:revision_id', 'field_civicrm_multiday_session:revision_id');
    $this->addFieldMapping('field_date_of_first_session:timezone');
    $this->addFieldMapping('field_date_of_first_session:rrule');
    $this->addFieldMapping('field_date_of_first_session:to');
    $this->addFieldMapping('field_civicrm_multidayevent_type', 'field_civicrm_multidayevent_type')
      ->sourceMigration('MetroEastMultidayEventTypeTerm')
      ->arguments(array('source_type' => 'tid'));
  }

  /**
   * Implements prepareRow().
   *
   * @param $row
   *
   * @return bool
   */
  public function prepareRow($row) {
    $row->type = 'civicrm_multiday_event';
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (is_array($row->field_civicrm_multiday_session)) {
      $row->sessions = array();
      foreach ($row->field_civicrm_multiday_session as $session) {
        db_set_active($this->sourceConnection);
        $query = db_query('SELECT * FROM {field_data_field_session_date} WHERE entity_id = :eid', array(':eid' => $session));
        $result = $query->fetchAssoc();
        if ($result) {
          $old_fc = field_collection_item_load($session);
          db_set_active();
          // create a new field_collection item from the result.
          $fc_item = entity_create('field_collection_item', array('field_name' => 'field_civicrm_multiday_session'));
          $fc_item->setHostEntity('node', $row);
          $fc_item->field_session_date = $old_fc->field_session_date;
          $fc_item->save(TRUE);
          $row->sessions[] = array(
            'value' => $fc_item->item_id,
            'revision_id' => $fc_item->revision_id,
          );
        }

      }
    }
    db_set_active();
    return TRUE;
  }

  /**
   * Implements prepare().
   *
   * @param $node
   * @param stdClass $row
   */
  public function prepare($node, stdClass $row) {
    $node->field_civicrm_multiday_session[LANGUAGE_NONE] = $row->sessions;
  }
}

class MetroEastBlogNodeMigration extends DrupalNode7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('upload', 'upload')
         ->sourceMigration('MetroEastBlogFiles');

    $this->addFieldMapping('upload:file_class')
         ->defaultValue('MigrateFileFid');

    $this->addFieldMapping('taxonomy_vocabulary_1', 'taxonomy_vocabulary_1')
             ->sourceMigration('MetroEastBlogUsersTerm')
             ->arguments(array('source_type' => 'tid'));

  }
}