<?php

/**
 * @file
 * CiviCRM Migrations
 */

class MetroEastCiviMigration extends DynamicMigration {
  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();
    $this->dependencies = array('MetroEastUser');
    // With migrate_ui enabled, migration pages will indicate people involved in
    // the particular migration, with their role and contact info. We default the
    // list in the shared class; it can be overridden for specific migrations.
    $this->team = array(
      new MigrateTeamMember('Daniel Sasser', 'daniel.sasser@gmail.com', t('Developer')),
    );
  }
}

class MetroEastCiviGroupSyncMigration extends MetroEastCiviMigration {


  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();


    $this->dependencies = array('MetroEastRole');
    $this->description = 'CiviCRM Group Role Sync table';
    $table_name = 'civicrm_group_roles_rules';
    $this->map = new MigrateSQLMap($this->machineName,
      array('id' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
             )
           ),
        MigrateDestinationTable::getKeySchema($table_name)
      );
    $query = Database::getConnection('default', 'live')
         ->select($table_name, 'c')
             ->fields('c', array('id', 'role_id', 'group_id'));
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationTable($table_name);
    // Mapped fields
    $this->addSimpleMappings(array('id', 'group_id'));

    $this->addFieldMapping('role_id', 'role_id')
          ->sourceMigration('MetroEastRole');
  }
}

class MetroEastCiviMemberSyncMigration extends DynamicMigration {
  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();

    $this->dependencies = array('MetroEastRole');
    $this->description = 'CiviCRM Member Role Sync table';
    $table_name = 'civicrm_member_roles_rules';

    $this->map = new MigrateSQLMap($this->machineName,
      array('rule_id' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
             )
           ),
        MigrateDestinationTable::getKeySchema($table_name)
      );
    $query = Database::getConnection('default', 'live')
         ->select($table_name, 'c')
         ->fields('c', array('rule_id', 'rid', 'type_id', 'status_codes'));
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationTable($table_name);
    // Mapped fields
    $this->addSimpleMappings(array('id', 'type_id', 'status_codes'));

    $this->addFieldMapping('rid', 'rid')
          ->sourceMigration('MetroEastRole');


  }
}

/**
 * Class MetroEastCiviCertifyRuleMigration
 */
class MetroEastCiviCertifyRuleMigration extends DynamicMigration {
  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();

    $this->team = array(
      new MigrateTeamMember('Daniel Sasser', 'daniel.sasser@gmail.com', t('Developer')),
    );

    $this->description = 'CiviCRM Certify Rule table migration';
    $table_name = 'certification_rule';
    $this->dependencies = array('MetroEastUser');
    $this->description = 'Copy the certification_rule table.';
    $destination_key = array(
      'cert_rule_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    );

    $fields = array('cert_rule_id', 'cert_rule_name', 'cert_rule_created',
            'cert_rule_event_type', 'cert_rule_status', 'cert_rule_group');
    $query = Database::getConnection('default', 'live')
      ->select($table_name, 'c')
      ->fields('c', $fields);

    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationTable('certification_rule');

    $this->map = new MigrateSQLMap($this->machineName,
    array('cert_rule_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'c',
      )
    ),
    $destination_key
    );

    $this->addSimpleMappings($fields);
  }
}

