<?php

/**
 * @file
 * migrate implementations file
 */


/**
* Implements hook_migrate_api
*
* @return array
*/
function metroeast_migrate_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'MetroEast' => array(
        'title' => t('MetroEast Migrations'),
        'default_format' => 'filtered_html',
      ),
    ),
    // these migrations are registered here because they are not
    // part of the migrate_d2d module
    'migrations' => array(
      'MetroEastCiviGroupSync' => array('class_name' => 'MetroEastCiviGroupSyncMigration'),
      'MetroEastCiviMemberSync' => array('class_name' => 'MetroEastCiviMemberSyncMigration'),
      'MetroEastCiviCertifyRule' => array('class_name' => 'MetroEastCiviCertifyRuleMigration'),
      'MetroEastBlock' => array('class_name' => 'MetroEastBlockMigration'),
    ),
  );
  return $api;
}