<?php

/**
 * Common mappings for the Drupal 7 user migrations.
 */
abstract class MetroeastUserMigration extends DrupalUser7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    
    $this->addFieldMapping('roles', 'roles')
      ->sourceMigration('MetroEastRole');
    
    $options = array(
      'md5_passwords' => 1,
      'language' => 'en',
      'text_format' => 'filtered_html'
    );
    $this->destination = new MigrateDestinationUser($options);
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('is_new')->defaultValue(TRUE);
    
  }
}