<?php

/**
* Implementation of hook_default_page_manager_pages().
*/
function metroeast_custom_default_page_manager_pages() {
  $pages = array();
  $page = new stdClass(); // prevents php debug warning
  $files = file_scan_directory(drupal_get_path('module', 'metroeast_custom'). '/pages', '/.page/');
  foreach($files as $file) {
    include_once $file->uri;
    $pages[$page->name] = $page;
  }
  return $pages;
}