<?php
/**
 * @file
 * metroeast_custom.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function metroeast_custom_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access CiviMember'.
  $permissions['access CiviMember'] = array(
    'name' => 'access CiviMember',
    'roles' => array(
      'administrator' => 'administrator',
      'staff user' => 'staff user',
    ),
    'module' => 'civicrm',
  );

  // Exported permission: 'access civicrm member role setting'.
  $permissions['access civicrm member role setting'] = array(
    'name' => 'access civicrm member role setting',
    'roles' => array(
      'administrator' => 'administrator',
      'staff user' => 'staff user',
    ),
    'module' => 'civicrm_member_roles',
  );

  // Exported permission: 'delete in CiviMember'.
  $permissions['delete in CiviMember'] = array(
    'name' => 'delete in CiviMember',
    'roles' => array(
      'administrator' => 'administrator',
      'staff user' => 'staff user',
    ),
    'module' => 'civicrm',
  );

  // Exported permission: 'edit memberships'.
  $permissions['edit memberships'] = array(
    'name' => 'edit memberships',
    'roles' => array(
      'administrator' => 'administrator',
      'staff user' => 'staff user',
    ),
    'module' => 'civicrm',
  );

  return $permissions;
}
